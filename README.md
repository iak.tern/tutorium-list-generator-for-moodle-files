This tool will create one excel table for every tutor and every of its assigned _Tutorium_ time slots. In every table it will create three lists:

1. a list with all students and every Tutorium. It's for the tutor to insert the collected points
2. an ISIS upload list. It contains the added points of every student and can be used to upload the points to ISIS. 
3. the print list. Can be used to be printed out for every tutorium so that the students can mark which task they did.

# Tool Setup:

1. Clone project
```
git clone https://gitlab.tubit.tu-berlin.de/snet/gepit-list-tool.git
```
2. Navigate into the project's directory
```
cd gepit-list-tool
```
3. Install dependencies
```
npm install
```

# Preparation:
1. Navigate into the `lists`-folder
```
cd files\lists
```
2. Copy the needed files into the directory (the name is not important):
  - ISIS list (directly from ISIS)
  - MOSES list (directly from MOSES)
  - Tutor list (see later section how to generate this list)

# Use: 
1. In the main folder run: `node app.js <lecture-name>` E.g.: `node app.js Geschäftsprozesse`
1. The program will ask you to select the corresponding files. Choose the correct ones. 
1. Navigate into the `tuts` folder: `cd ../files/tuts` 
Here you can find the resulting tables

# Tutor list format:
Create a file and store it as `<filename>.csv`:

```
Tutorname 1, "Slot1",  "Slot2"
Tutorname 2, "Slot1",  "Slot2", "Slot3",  "Slot4"
Tutorname 3, "Slot1",  "Slot2", "Slot3"
...
```

_Slot_-Syntax (can be copies from the MOSES list: its the end of the "Tutorium" column): `Day From - To, Room` e.g. `Mi 18:00 - 20:00, MAR 4063`

Example of a real `tutors.csv`:

```
Alexander,"Do 12:00 - Do 14:00, FH 303","Do 16:00 - Do 18:00, FH 312","Do 18:00 - Do 20:00, FH 315"
Sebastian,"Do 16:00 - Do 18:00, FH 311","Do 18:00 - Do 20:00, FH 316",
Wolf,"Fr 08:00 - Fr 10:00, FH 316","Fr 12:00 - Fr 14:00, FH 302",
Yannick,"Fr 08:00 - Fr 10:00, FH 314","Fr 12:00 - Fr 14:00, FH 303","Fr 14:00 - Fr 16:00, FH 313"
```
 
