//for creating the tables
const {table} = require('table');

//file system
const fs = require('fs');

//synchronous console input
var prompt = require('prompt-sync')();

//csv parsers
var csv = require("fast-csv");

//xlsx parser
const XlsxPopulate = require('xlsx-populate');

let courseName = "Webtechnologien"

//predefined paths -> may be overwritten by the user
var defaultInputPath = 'files/lists';
var defaultOutputPath = 'files/tuts/';

//paths to the initial lists
var mosesPath = "";
var isisPath =  "";
var tutsPath = "";

//head of all ISIS tables - maybe find another solution
var tableHead = [];

if( process.argv[2] !== undefined){
    courseName = process.argv[2];
}

console.log(    `The input folder is:  ${process.cwd()}/${defaultInputPath}
Make sure the input folder includes:
\t 1. A list of attendees, provided by MOSES
\t 2. A list of attendees, provided by ISIS
\t 3. A list assigning tutors to time slots`);


//reads all files in the "lists" folder
//user chooses the prompted files
files= fs.readdirSync(defaultInputPath);

//output of all files in the folder
var count = 0;
console.log('\n');
files.forEach(file => {
    console.log(count++ + ". " + file);
});

//user chooses files
var prefix= defaultInputPath + '/';

console.log('\n');
console.log('Choose the lists by individually entering their indexes (seen above) and confirming with the return key.')

while(true){
    console.log('\n');
    var isisID = prompt('Index of ISIS list path: ');
    var mosesID = prompt('Index of MOSES list path: ');
    var tutsID = prompt('Index of Tut-List path: ');
    var exit = prompt('All good? Y/N: ');

    mosesPath= prefix + files[mosesID];
    isisPath= prefix + files[isisID];
    tutsPath= prefix + files[tutsID];

    if(exit === "Y"){
        break;
    }
}

//files are read and we keep the headers
mosesList= fs.readFileSync(mosesPath,'utf8');
mosesList = csvParse(mosesList).slice(5);
mosesList = mosesList.map(l=>removeQuotationMarks(l));


isisList= fs.readFileSync(isisPath,'utf8');
isisList = csvParse(isisList);
isisList = isisList.map(l=>removeQuotationMarks(l));
tableHead= isisList[0];

tutList = fs.readFileSync(tutsPath, 'utf-8');
tutList = csvParse(tutList);
createFolders();



var joined=joinArrays(mosesList,isisList);
console.log(isisList[0]);

//finding all timeslots
//all timeslots for tutorials
var timeslots= new Set();
joined.forEach(arr=>{
    timeslots.add(arr[11]);
})
timeslots= Array.from(timeslots);
console.log(timeslots);

//object containing all lists. fields are called by timeslots, 
var lists = {};


//filling the lists object with the lists
timeslots.forEach(slot=>{
    let list = [];
    for(var i = 0; i!=joined.length; i++){
        if(joined[i][11]===slot){
            list.push(joined[i]);
        }
    }
    lists[slot]=list;
}) 

lists= removeTuts(lists);

createLists(lists);

console.log('\n \n');
console.log('Lists have been generated.')

/**
 * Removes quotation marks from all strings in a list
 * @param {list} list a list containing strings 
 * @returns {list} 
 */
function removeQuotationMarks(list){
    return list.map(str=>str.replace(/"/g,""));
}


/**
 * checks if the necessary folders for each tutor exist and if not, creates them
 */
function createFolders(){
    tutList.forEach(l=>{
        if (!fs.existsSync(defaultOutputPath+l[0])){
            fs.mkdirSync(defaultOutputPath+l[0]);
        }
    })
}

/**
 * Searches for the tutor of a given timeslot
 * @param {string} slot a tutorial's timeslot
 */
function findTutor(slot){
    for(var i = 0; i!=tutList.length; i++){
        for(var j = 1; j!=tutList[i].length; j++){
            if(tutList[i][j]== "\"" +slot + "\""){          //TODO FIX THIS
                return tutList[i][0];
            }
        }
    }
}


/**
 * Removes the tutorial field from all students in our lists
 * @param {object} listObject is an object containing all lists of students for every tutorial
 * @returns {object} same object as passed in, but for each student the tutorial is removed
 */
function removeTuts(listObject){
    Object.keys(listObject).forEach(function(key) {
        listObject[key].map(list=>{
            return list.splice(-1,1);
        }); 
    });

    return listObject;
}


/**
 * Creates a new xlsx document with 3 sheets
 * - sheet that can be uploaded to ISIS
 * - sheet in which you can enter the achieved points for individual tutorials
 * - sheet to print
 * @param {object} lists object containing all lists
 */
function createLists(lists){
    Object.keys(lists).forEach(function(key) {
        //looks up for which tutor we are creating a list
        var tutor = findTutor(key);
        //filenames cannot contain these symbols
        var filename = key.split(":").join("").split(",").join("");

        //as we will need multiple table heads, we create one from the "template" we extracted above
        var head = Array.from(tableHead);
         
        //for the first sheet, we don't need these fields. backing them up so we can add it later (easier than switching sheet creation, but ugly. maybe find a better way)
        var matrNrs = []
        var nameV = [];
        var nameN = [];

        lists[key].forEach(student=>{
            nameN.push(student[1]);
            student.splice(1,1);
            nameV.push(student[1]);
            student.splice(1,1);
            matrNrs.push(student[1]);
            student.splice(1,1);
        });

        //adding the head for the first sheet
        lists[key].unshift(head);

        XlsxPopulate.fromBlankAsync()
        .then(workbook => {

            

            // created ISIS Upload List
            //const upload = workbook.sheet(0).cell("A1").value(lists[key]);
            const allTuts = workbook.sheet(0).name("Tutorien_Gesamt");

            //create Table containing all Tuts
            workbook.addSheet("ISIS_Upload");
            workbook.sheet(1).cell("A1").value(lists[key]);

            //adding cols for all 20 potential tutorials and filling the fields with empty strings
            lists[key].forEach(student =>{
                for(var j = 1; j!=21;j++){
                    student.push("");
                }
            })
            
            var newHead = Array.from(tableHead);
            for(var i = 1; i!=21;i++){
                newHead.push("Tutorium " + i);
            }
            
            lists[key][0] = newHead;

            //removing unnecessary fields that we needed in sheet 1, but not sheet 0
            lists[key].forEach(student=>{
                student.splice(0,1);
                student.splice(2,1);
                student.splice(4,1);
                student.splice(4,1);
            });


            //renaming columns
            lists[key][0][2] = "gesammelte Punkte";
            lists[key][0][3] = "Maximalpunktzahl";

            //adding the backed up columns
            lists[key][0].splice(1,0,"Nachname");
            lists[key][0].splice(2,0,"Vorname");
            lists[key][0].splice(3,0,"MatrNr.");

            //add new fields (matrNr, name, vname)
            for(i= 1; i!=lists[key].length;i++){
                lists[key][i].splice(1,0,nameN[i-1]);
                lists[key][i].splice(2,0,nameV[i-1]);
                lists[key][i].splice(3,0,matrNrs[i-1]);
            }

            allTuts.cell("A3").value(lists[key]);

            //add formulas and styles to the sheets
            for(var k= 2; k!=lists[key].length+1;k++){
                workbook.sheet(1).row(k).cell(5).formula("Tutorien_Gesamt!F"+(k+2));
                workbook.sheet(0).row((k+2)).cell(6).formula("SUM(H"+(k+2)+":AA"+(k+2)+")");
            }
            workbook.sheet(0).cell("F1").value("Punkte bisher:");
            workbook.sheet(0).row(1).cell(7).formula("SUM(H1:AA1)");

            //hardcoded :( -> not all styles seem to work as intended. no way to set width to auto with this library

            //upload list
            workbook.sheet(1).column(1).width(20);
            workbook.sheet(1).column(2).width(30);
            workbook.sheet(1).column(3).width(40);
            workbook.sheet(1).column(4).width(20);
            workbook.sheet(1).column(5).width(12);
            workbook.sheet(1).column(6).width(16);
            workbook.sheet(1).column(7).width(32);
            workbook.sheet(1).column(8).width(26);

            //all tut list
            workbook.sheet(0).column(1).width(30);
            workbook.sheet(0).column(2).width(20);
            workbook.sheet(0).column(3).width(20);
            workbook.sheet(0).column(4).width(20);
            workbook.sheet(0).column(5).width(40);
            workbook.sheet(0).column(6).width(20);
            workbook.sheet(0).column(7).width(20);
            for(i=8;i!=28;i++){
                workbook.sheet(0).column(i).width(12);
            }


            //creating List to Print
            const printList = workbook.addSheet("Print_List");

            printList.cell("A1").value(courseName);
            printList.column("A").width(40);
            printList.cell("A2").value(tutor+": " + key);

            //removing all cols but the name one
            lists[key].forEach(student=>{
                student.splice(1,26);
            });

            //adding 7 columns for potential tasks
            for(var i = 1; i!=8;i++){
                lists[key][0].push("A"+i);
            }

            printList.cell("A4").value(lists[key]).style("border", true);;

            for(var j = 4;j!=lists[key].length+4;j++){
                workbook.sheet(2).row(j).height(20);
            }

            for(var k = 2;k!=9;k++){
                workbook.sheet(2).column(k).width(6);
            }

        
            return workbook.toFileAsync(defaultOutputPath + tutor + "/" + filename + "_Table.xlsx");
    });
       

    });
}


/**
 * Joins the rows of both lists if the name matches
 * Adds the names and student ID so we can use them later on in the list creation process
 * @param {Array} arr1 MOSES list
 * @param {Array} arr2 ISIS list
 * @return {Array} 
 */
function joinArrays(arr1, arr2){
    var joined = [];

    arr1.forEach(arr=>{
        var name= arr[1] + " " + arr[0];
        for(var i=0;i!=arr2.length;i++){
            if(arr2[i][1]==name){
                arr2[i].push(arr[6].match(/(M|D|F)(o|i|r) (0|1|2)[0-8]:00.{12,30}/g)[0]); // FIXME !!!!!!!! This is BS! not a great regex but will do the trick
                arr2[i].splice(1,0,arr[0]);
                arr2[i].splice(2,0,arr[1]);
                arr2[i].splice(3,0,arr[2]);
                joined.push(arr2[i]);
            }
        }
    })

    return joined;
}


/**
 * Takes a csv string and returns an array of arrays representing rows and the individual cells in the rows
 * @param {String} csv a string read from a CSV file
 * @return {Array} array of arrays. outer array contains the rows, inner array the columns of the csv
 */
function csvParse(csv){
    var res= csv.split("\n").map(function(row){
        row = row.replace(',,',',"",' ); //kinda still hard coded. improve this
        row = row.replace(',,',',"",' );
        return row.match(/(".*?"|[^",\s]+)(?=\s*,|\s*$)/g); //doesn't match commas in double quotes https://stackoverflow.com/questions/11456850
    }); 
    res.pop();//regex had some issues and added a last element that didn't belong there
    return res;
}

module.exports = {
    csvParse: csvParse,
    joinArrays: joinArrays,
    findTutor: findTutor,
    createFolders: createFolders,
    createLists: createLists,
    removeQuotationMarks: removeQuotationMarks
}